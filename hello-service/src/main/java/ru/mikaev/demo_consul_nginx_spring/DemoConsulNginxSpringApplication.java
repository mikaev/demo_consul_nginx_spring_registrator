package ru.mikaev.demo_consul_nginx_spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoConsulNginxSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoConsulNginxSpringApplication.class, args);
    }
}
